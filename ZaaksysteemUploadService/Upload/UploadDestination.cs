﻿#region Software License
/*
    Copyright (c) 2015, Mintlab B.V. and all the persons listed in the
    CONTRIBUTORS file.

    Zaaksysteem Upload Service uses the EUPL license, for more information
    please have a look at the LICENSE file.
*/
#endregion

namespace ZaaksysteemUploadService
{
    public struct UploadableFile
    {
        public string Filename { get; set; }
        public UploadDestination UploadDestination { get; set; }
    }

    public struct UploadDestination
    {
        public string APIKey { get; set; }
        public string Name { get; set; }
        public string PostURL { get; set; }
    }
}