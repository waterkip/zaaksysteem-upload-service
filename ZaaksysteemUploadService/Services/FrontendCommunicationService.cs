﻿#region Software License
/*
    Copyright (c) 2015, Mintlab B.V. and all the persons listed in the
    CONTRIBUTORS file.

    Zaaksysteem Upload Service uses the EUPL license, for more information
    please have a look at the LICENSE file.
*/
#endregion

using System.Collections.Generic;
using System.Security.Permissions;
using System.ServiceModel;
using ZaaksysteemUpload;

namespace ZaaksysteemUploadService
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    class FrontendCommunicationService : IConfigurationService
    {
        // Well-known principal "Local administrator"
        private const string LocalAdministrator = "S-1-5-32-544";

        public delegate void ConfigurationChangeNotification();
        internal ConfigurationChangeNotification OnConfigurationChange;
        internal ServiceConfiguration ServiceConfiguration;

        [PrincipalPermission(SecurityAction.Demand, Role = LocalAdministrator)]
        public ConfigurationData GetConfiguration()
        {
            var ConfigItems = ServiceConfiguration.GetConfigurationItems();

            return new ConfigurationData
            {
                ConfigurationItems = ConfigItems,
                HTTPTimeoutMinutes = ServiceConfiguration.HTTPTimeoutMinutes
            };
        }

        [PrincipalPermission(SecurityAction.Demand, Role = LocalAdministrator)]
        public void SetConfiguration(ConfigurationData config)
        {
            ServiceConfiguration.SetConfiguration(config);

            OnConfigurationChange();
            return;
        }
    }
}
