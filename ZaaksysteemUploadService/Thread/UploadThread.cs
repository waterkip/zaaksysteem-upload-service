﻿#region Software License
/*
    Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the
    CONTRIBUTORS file.

    Zaaksysteem Upload Service uses the EUPL license, for more information
    please have a look at the LICENSE file.
*/
#endregion

using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace ZaaksysteemUploadService
{
    public class UploadThread
    {
        public ILogger Logger;
        public CancellationToken CancellationToken;
        internal BlockingCollection<UploadableFile> UploadQueue;
        internal ServiceConfiguration ServiceConfiguration;
        internal string Version;

        public void Run()
        {
            Logger.DebugEntry("Started new upload thread");
            try {
                foreach (UploadableFile file in UploadQueue.GetConsumingEnumerable(CancellationToken))
                {
                    using (var client = new HttpClient())
                    {
                        client.DefaultRequestHeaders.Add("User-Agent", "ZaaksysteemUploadService/" + Version);
                        client.Timeout = TimeSpan.FromMinutes(ServiceConfiguration.HTTPTimeoutMinutes);

                        var task = UploadFile(client, file);
                        task.Wait();
                    }
                }
            }
            catch(OperationCanceledException)
            {
                // That's expected. Do nothing.
            }

            return;
        }

        public void DeleteFile(UploadableFile file)
        {
            Logger.DebugEntry("Deleting " + file.Filename);
            try
            {
                File.Delete(file.Filename);
            }
            catch (Exception e)
            {
                var message = string.Format(
                    "File '{0}' could not be removed.\nException: {1}\nError: {2}",
                    file.Filename, e.GetType(), e.Message
                );
                Logger.WriteEntry(message, EventLogEntryType.Error);
            }

            return;
        }

        public async Task UploadFile(HttpClient client, UploadableFile file)
        {
            var basename = Path.GetFileName(file.Filename);

            Logger.WriteEntry(string.Format(
                "[{0}] Uploading '{1}' to '{2}'",
                file.UploadDestination.Name,
                file.Filename,
                file.UploadDestination.PostURL
            ));

            // Set the base URL for uploading to
            client.BaseAddress = new Uri(file.UploadDestination.PostURL);

            using (var content = new MultipartFormDataContent())
            {
                // The API key form field
                content.Add(new StringContent(file.UploadDestination.APIKey), "api_key");

                // The actual file data
                using (var fileStream = WaitForFile(file.Filename, FileMode.Open, FileAccess.Read, FileShare.Delete))
                {
                    if (fileStream == null)
                    {
                        var message = string.Format(
                            "Could not get an exclusive lock on '{0}'.",
                            basename
                        );
                        Logger.WriteEntry(message, EventLogEntryType.Error);

                        return;
                    }

                    using (var stream = new StreamContent(fileStream))
                    {
                        // We use our own Content-Disposition header, as Catalyst doesn't like the default
                        // (which has a "filename*=utf8" field at the end)
                        stream.Headers.ContentType = MediaTypeHeaderValue.Parse("application/octet-stream");
                        stream.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
                        {
                            Name = "filename",
                            FileName = basename
                        };
                        content.Add(stream);

                        Logger.DebugEntry("Sending POST to " + client.BaseAddress + "api/scanstraat/upload_document");
                        try
                        {
                            HttpResponseMessage response = await client.PostAsync(
                                "api/scanstraat/upload_document", content
                            );
                            response.EnsureSuccessStatusCode();
                        }
                        catch (Exception e)
                        {
                            string message;
                            if (e is HttpRequestException)
                            {
                                message = string.Format(
                                    "File '{0}' could not be uploaded (HTTP request failed).\nError: {1}",
                                    basename, e.Message
                                );
                            }
                            else if (e is TaskCanceledException)
                            {
                                message = string.Format(
                                    "File '{0}' could not be uploaded (probably a HTTP client timeout).\nError: {1}",
                                    basename, e.Message
                                );
                            }
                            else
                            {
                                message = string.Format(
                                    "File '{0}' could not be uploaded.\nException: {1}\nError: {2}",
                                    basename, e.GetType(), e.Message
                                );
                            }

                            Logger.WriteEntry(message, EventLogEntryType.Error);
                            return;
                        }
                    }
                }

                Logger.WriteEntry(string.Format(
                    "File '{0}' uploaded successfully.",
                    basename
                ));

                DeleteFile(file);
            }
        }

        FileStream WaitForFile(string fullPath, FileMode mode, FileAccess access, FileShare share)
        {
            if (Directory.Exists(fullPath))
            {
                Logger.DebugEntry("File '" + fullPath + "' is a directory. Refusing to open.");

                return null;
            }

            Logger.DebugEntry("Waiting for file " + fullPath + " to unlock");

            // Wait for 60 * 500ms = 30000ms = 30s maximum.
            // This is *after* the last "file changed" event.
            const int maxTries = 60;
            const int tryInterval = 500;

            for (int numTries = 0; numTries < maxTries; numTries++)
            {
                try
                {
                    FileStream fs = new FileStream(fullPath, mode, access, share);

                    fs.ReadByte();
                    fs.Seek(0, SeekOrigin.Begin);

                    Logger.DebugEntry("Done sleeping on " + fullPath + " - returning handle");

                    return fs;
                }
                catch (IOException)
                {
                    Logger.DebugEntry("Waiting for file (sleep now) " + fullPath);

                    Thread.Sleep(tryInterval);
                }
            }

            return null;
        }
    }
}
