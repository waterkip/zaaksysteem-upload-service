# Zaaksysteem Upload Service

This is a companion project to [Zaaksysteem](http://bitbucket.org/mintlab/zaaksysteem).


The repository contains a Visual Studio (2015) Solution with several
related projects:

## ZaaksysteemUploadService
"ZaaksysteemUploadService" is a service that watches a directory for new
files, and uploads those to the configured Zaaksysteem instance, using
a shared secret.

## ZaaksysteemUploadConfigurator
To configure this service, the "ZaaksysteemUploadConfigurator" tool
is a simple WinFroms application that uses WCF to communicate with the
service to read and store configuration data.

## ZaaksysteemUpload
The "ZaaksysteemUpload" project contains some WCF classes and interfaces
that are shared between the service and the configuration tool.

## ZaaksysteemUploadSetup
Last, there's "ZaaksysteemUploadSetup", which generates a .msi file and
Setup.exe, either of which can be used to install the service. It is not
built by default

# Patches

The main version of this project is maintained on [Bitbucket](https://bitbucket.org/mintlab/zaaksysteemuploadservice).

If you have improvements to this project, please make a fork of your own and
submit a pull request.