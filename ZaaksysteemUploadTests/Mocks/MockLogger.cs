﻿#region Software License
/*
    Copyright (c) 2015, Mintlab B.V. and all the persons listed in the
    CONTRIBUTORS file.

    Zaaksysteem Upload Service uses the EUPL license, for more information
    please have a look at the LICENSE file.
*/
#endregion

using System.Collections.Generic;
using System.Diagnostics;
using ZaaksysteemUploadService;

namespace ZaaksysteemUploadTests
{
    struct LogMessage
    {
        public string message;
        public EventLogEntryType type;
    }

    class MockLogger : ILogger
    {
        public List<LogMessage> LoggedMessages { get; set; }

        public MockLogger()
        {
            LoggedMessages = new List<LogMessage>();
        }

        public void DebugEntry(string message)
        {
            LoggedMessages.Add(new LogMessage() { message = message });
        }

        public void WriteEntry(string message)
        {
            WriteEntry(message, EventLogEntryType.Information);
        }

        public void WriteEntry(string message, EventLogEntryType type)
        {
            LoggedMessages.Add(new LogMessage() { message = message, type = type });
        }
    }
}
